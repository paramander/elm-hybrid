{-# LANGUAGE DeriveGeneric #-}

module Main where

import           Data.Time
import           Elm
import           GHC.Generics

data Sum = One | Two
    deriving (Show, Generic)

data Record = Record
    { recordString     :: String
    , recordInt        :: Int
    , recordFloat      :: Float
    , recordCustom     :: Sum
    , recordList       :: [String]
    , recordNestedList :: [[String]]
    }
  deriving (Show, Generic)

data Wrap = Wrap
    { wrapRecord :: Record
    , wrapDate   :: UTCTime
    }
    deriving (Show, Generic)

instance ToElm Sum

instance ToElm Record where
    toElm = genericToElm defaultOptions
        { jsonSelectorModifier = drop 6
        , recordSelectorModifier = drop 6
        }

instance ToElm Wrap

main :: IO ()
main = do
    generateElm "dist" "Gen.Sum" []
        [ (toElm (undefined :: Sum), [SumType, Decoder, Encoder])
        ]
    generateElm "dist" "Gen.Record" ["Gen.Sum"]
        [ (toElm (undefined :: Record), [Type, Decoder, Encoder])
        ]
    generateElm "dist" "Gen.Wrap" ["Gen.Record"]
        [ (toElm (undefined :: Wrap), [Type, Decoder, Encoder])
        ]
