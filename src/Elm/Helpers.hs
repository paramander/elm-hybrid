module Elm.Helpers
    ( tab
    , surround
    , format
    ) where

import           Data.List
import           Prelude

tab :: Int -> String -> String
tab times = (++) (take times $ repeat ' ')

surround :: String -> String -> String -> [String] -> [String]
surround a b c (d:ds) = (a ++ d) : (map ((++) b) ds) ++ [c]
surround a _ c _ = a : c : []

format :: [String] -> String
format = concat . intersperse "\n"
